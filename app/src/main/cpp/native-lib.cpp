#include <jni.h>
#include <string>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;
const int FRAME_WIDTH = 320;
const int FRAME_HEIGHT = 240;
const int MIN_OBJECT_AREA = 25 * 25;
const int MAX_OBJECT_AREA = FRAME_HEIGHT*FRAME_WIDTH / 1.5;
int Max_obj = 80;
int xm, ym;
int Hmin = 0;
int Smin = 100;
int Vmin = 100;
int Hmax = 10;
int Smax = 255;
int Vmax = 255;



extern "C" {
JNIEXPORT void JNICALL Java_org_cocos2dx_cpp_AppActivity_FunctionName(JNIEnv * env, jobject obj);
jstring
Java_com_example_jackyle_opencvtesting_MainActivity_stringFromJNI(JNIEnv *env, jobject /* this */) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
std::string int2str(int number) {
    std::stringstream ss;
    ss << number;
    return ss.str();
}
//int mainProcess(Mat srcImg, Mat& resultImage, int Hmin,int Smin, int Vmin, int Hmax, int Smax, int Vmax )
int mainProcess(Mat srcImg, Mat& resultImage){
    Mat hvsImg;
    Mat inrImg;
    cvtColor(srcImg, hvsImg, COLOR_BGR2HSV);	// Convert Color image to HSV image ( 3 color: H,S and V)
    inRange(hvsImg, Scalar(Hmin, Smin, Vmin), Scalar(Hmax, Smax, Vmax), inrImg); // Filter obj base on color.

    Mat erode_element = getStructuringElement(MORPH_RECT, cv::Size(3, 3));
    Mat dilate_element = getStructuringElement(MORPH_RECT, cv::Size(8, 8));

    erode(inrImg, inrImg, erode_element);
    erode(inrImg, inrImg, erode_element);

    dilate(inrImg, inrImg, dilate_element);
    dilate(inrImg, inrImg, dilate_element);

    Mat temps;
    inrImg.copyTo(temps);
    inrImg.copyTo(resultImage);

    vector<vector<cv::Point>> conts;
    vector<Vec4i> hier;
    findContours(temps,conts,hier,CV_RETR_CCOMP,CV_CHAIN_APPROX_SIMPLE);	//Find contours

    double arearef = 0;
    bool objFound = false;
    if (hier.size() > 0)	// hier: number of object detected after contours processing.
    {
        int obj_qty = hier.size();
        if (obj_qty < Max_obj)	//Only process when number of obj lower than define max obj.
        {
            for (int idx = 0; idx >= 0; idx = hier[idx][0]) {
                Moments moms = moments((cv::Mat)conts[idx]);
                double area = moms.m00;
                if (area > MIN_OBJECT_AREA && area < MAX_OBJECT_AREA && area >= arearef) // if area in range we defined -> obj found
                {
                    xm = moms.m10 / area;
                    ym = moms.m01 / area;
                    objFound = true;
                    arearef = area;
                }
                else
                {
                    objFound = false;
                }
            }
        }
        if (objFound == true)
        {
            putText(srcImg, "Object Found", cv::Point(0, 50), 2, 3, Scalar(0, 255, 0), 2); // Put text on the video
            circle(srcImg, cv::Point(xm, ym), 50, Scalar(0, 255, 0), 2, 8, 0);
            putText(srcImg, int2str(xm) + int2str(ym) , cv::Point(xm, ym), 2, 3, Scalar(0, 255, 0), 2);
        }
        else
        {
            putText(srcImg, "Object Not Found", cv::Point(0, 50), 2, 3, Scalar(255, 0, 0), 2); // Put text on the video
        }
    }
    return 1;
//    cvtColor(img,gray,CV_RGBA2GRAY);
//    if(gray.rows == img.rows && gray.cols == img.rows)
//    { return 1;}
//    else
//    {
//        return 0;
//    }
}
//,jint jHmin, jint jSmin, jint jVmin, jint jHmax,jint jSmax,jint jVmax
jint
Java_com_example_jackyle_opencvtesting_MainActivity_imgProcess(JNIEnv *, jclass, jlong inputMat, jlong imageGray/* this */) {
    Mat& mRgb = *(Mat*) inputMat;
    Mat& mGray = *(Mat*) imageGray;
JNIEXPORT void Java_org_cocos2dx_cpp_AppActivity_FunctionName(JNIEnv* env, jobject obj)
{
 //   log(“Function Called”);
}
//    int& mHmin = *(int*) jHmin;
//    int& mSmin = *(int*) jSmin;
//    int& mVmin = *(int*) jVmin;
//    int& mHmax = *(int*) jHmax;
//    int& mSmax = *(int*) jSmax;
//    int& mVmax = *(int*) jVmax;

    int conv;
    jint retVal;
    conv = mainProcess(mRgb,mGray);
    retVal = (jint)conv;
    return retVal;
    //,mHmin,mSmin,mVmin,mHmax,mSmax,mVmax
}



}

